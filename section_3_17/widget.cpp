#include "widget.h"
#include "ui_widget.h"
#include "QDebug"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    // Connection, method 1, string notation.
    // connect(ui->pushButton,SIGNAL(clicked()), this,SLOT(changeMyText()));

    // Connection method 2, functor notation. // This does work!
    // connect(ui->pushButton,&QPushButton::clicked,this,&Widget::changeMyText);

     // Connection method 3, lambdas.
    /*
     * This only works if you select Qt5 (instead of Qt4)
     * and set this in your .pro file:
     * QMAKE_CXXFLAGS = -std=c++14
     * QMAKE_LFLAGS = -std=c++14
     */
     connect(ui->pushButton,&QPushButton::clicked,[=](){
        ui->label->setText("User has clicked!");
     });

}

Widget::~Widget()
{
    delete ui;
}

void Widget::changeMyText()
{
    qDebug() << "User clicked the button";
    ui->label->setText("User Clicked!");
}
