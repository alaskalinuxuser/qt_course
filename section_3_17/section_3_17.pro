#-------------------------------------------------
#
# Project created by QtCreator 2018-06-22T09:27:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = section_3_17
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui

QMAKE_CXXFLAGS = -std=c++14
QMAKE_LFLAGS = -std=c++14
