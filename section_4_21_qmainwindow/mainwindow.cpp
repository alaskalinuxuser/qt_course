#include "mainwindow.h"
#include "QPushButton"
#include "QDebug"
#include "QMenuBar"
#include "QStatusBar"
#include "QAction"
#include "QApplication"
#include "QSize"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QPushButton * myButton = new QPushButton("Howdy",this);

    // By setting the central widget, it will scale this to fill the center.
    setCentralWidget(myButton);

    // Now some QActions....
    // Quiting
    QAction * quitAction = new QAction("Quit", this);
    // And connect it.
    connect (quitAction,&QAction::triggered,[=](){
        QApplication::quit();
    });

    // Add our menu bar with some menu options.
    QMenu * fileMenu = menuBar()->addMenu("File");
    fileMenu->addAction(quitAction);

    QMenu * editMenu = menuBar()->addMenu("Edit");
    QMenu * settingsMenu = menuBar()->addMenu("Settings");
    QMenu * helpMenu = menuBar()->addMenu("Help");

    // Add a status bar with a message. You don't have to use the time
    // but it will last 5000 milliseconds in this set up.
    statusBar()->showMessage("This is our status message.",5000);
    // Clear the status bar.
    //statusBar()->clearMessage();



}

MainWindow::~MainWindow()
{

}

QSize MainWindow::sizeInt() const
{
 return QSize(800,800); // This is not working, and I don't know why.
}
