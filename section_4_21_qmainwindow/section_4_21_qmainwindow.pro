#-------------------------------------------------
#
# Project created by QtCreator 2018-06-26T09:47:57
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = section_4_21_qmainwindow
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++14


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h
