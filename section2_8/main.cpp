#include <iostream>

using namespace std;

int main()
{

    int a = 11;
    int b = 5;

//    cout << "----------MATHMATICAL OPERATORS---------" << endl;
//    cout << "a + b is: " << a+b << endl;
//    cout << "a - b is: " << a-b << endl;
//    cout << "a * b is: " << a*b << endl;
//    // To get decimal, we need to cast to a float first!
//    cout << "a / b is: " << (float) a/b << endl;
//    cout << "Remainder of a / b is: " << a%b << endl;

    cout << "----------LOGICAL OPERATORS---------" << endl;
    if (a < b) {
        cout << "a is less than b." << endl;
    } else if (a > b) {
        cout << "a is greater than b." << endl;
    } else {
        cout << "a is equal to b." << endl;
    }

    //Loops

    // For loop.

    for (int i = 1; i <= 20; i++) {
        cout << "Howdy " << i << endl;
    }

    int j = 1;
    while (j <= 20) {
        cout << "Howdy again! " << j << endl;
        j++;
    }


    return 0;
}

