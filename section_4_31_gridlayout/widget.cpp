#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    QGridLayout * myGrid = new QGridLayout(this);


    myGrid->addWidget(ui->label,0,0);
    myGrid->addWidget(ui->label_2,0,1);
    myGrid->addWidget(ui->label_3,0,2);
    myGrid->addWidget(ui->label_4,1,0);
    myGrid->addWidget(ui->label_5,1,1);
    myGrid->addWidget(ui->label_6,1,2);
    myGrid->addWidget(ui->label_7,2,0);
    myGrid->addWidget(ui->label_8,2,1);
    myGrid->addLayout(ui->gridLayout_2,2,2);
    setLayout(myGrid);

}

Widget::~Widget()
{
    delete ui;
}
