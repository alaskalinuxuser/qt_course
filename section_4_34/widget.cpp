#include "widget.h"
#include "ui_widget.h"
#include "QDebug"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    ui->comboBox->addItem("Navy");
    ui->comboBox->addItem("Army");
    ui->comboBox->addItem("Marines");
    ui->comboBox->addItem("Coast Guard");
    ui->comboBox->addItem("Air Force");

    ui->comboBox->setEditable(true);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_captureButton_clicked()
{
    qDebug() << "Current selection is: " << ui->comboBox->currentText();
    qDebug() << "Current index is: " << ui->comboBox->currentIndex();
}

void Widget::on_setButton_clicked()
{
    ui->comboBox->setCurrentIndex(0);
}

void Widget::on_getButton_clicked()
{
    qDebug() << "Complete combo box has: " << QString::number(ui->comboBox->count()) << " items.";

    for (int i=0; i< ui->comboBox->count(); i++){
    qDebug() << "Current selection is: " << ui->comboBox->itemText(i);
    }
}
