#include "widget.h"
#include "ui_widget.h"
#include "QButtonGroup"
#include "QDebug"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    QButtonGroup * exclusiveGroup = new QButtonGroup(this);
    exclusiveGroup->addButton(ui->macCheck);
    exclusiveGroup->addButton(ui->winCheck);
    exclusiveGroup->addButton(ui->linuxCheck);

    exclusiveGroup->setExclusive(true);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_linuxCheck_toggled(bool checked)
{
    if (checked){
    qDebug() << "Linux Checkbox is checked!";
    } else {
        qDebug() << "Linux Checkbox is unchecked!";
    }
}

void Widget::on_androidCheck_toggled(bool checked)
{
    if (checked){
    qDebug() << "Android Checkbox is checked!";
    } else {
        qDebug() << "Android Checkbox is unchecked!";
    }
}

void Widget::on_oldButton_toggled(bool checked)
{
    if (checked){
    qDebug() << "Old radio button is checked!";
    } else {
        qDebug() << "Old radio button is unchecked!";
    }
}

void Widget::on_applyButton_clicked()
{
    if (ui->iosCheck->isChecked()) {
        qDebug() << "iOS is checked!";
    } else {
        qDebug() << "iOS is unchecked!";
    }
}

void Widget::on_fillButton_clicked()
{
    if (ui->androidCheck->isChecked()) {
        ui->androidCheck->setChecked(false);
    } else {
        ui->androidCheck->setChecked(true);
    }

    if (ui->macCheck->isChecked()) {
        // Since they are exclusive, you need to set something else, not uncheck it.
        ui->linuxCheck->setChecked(true);
    } else {
        ui->macCheck->setChecked(true);
    }
}
