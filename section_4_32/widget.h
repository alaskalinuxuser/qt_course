#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private slots:
    void on_linuxCheck_toggled(bool checked);

    void on_androidCheck_toggled(bool checked);

    void on_oldButton_toggled(bool checked);

    void on_applyButton_clicked();

    void on_fillButton_clicked();

private:
    Ui::Widget *ui;
};

#endif // WIDGET_H
