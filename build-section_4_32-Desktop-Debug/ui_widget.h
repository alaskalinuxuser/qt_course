/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout_4;
    QPushButton *fillButton;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_3;
    QRadioButton *newButton;
    QRadioButton *oldButton;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *androidCheck;
    QCheckBox *iosCheck;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QCheckBox *macCheck;
    QCheckBox *linuxCheck;
    QCheckBox *winCheck;
    QPushButton *applyButton;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(172, 355);
        verticalLayout_4 = new QVBoxLayout(Widget);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        fillButton = new QPushButton(Widget);
        fillButton->setObjectName(QStringLiteral("fillButton"));

        verticalLayout_4->addWidget(fillButton);

        groupBox_3 = new QGroupBox(Widget);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_3 = new QVBoxLayout(groupBox_3);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        newButton = new QRadioButton(groupBox_3);
        newButton->setObjectName(QStringLiteral("newButton"));

        verticalLayout_3->addWidget(newButton);

        oldButton = new QRadioButton(groupBox_3);
        oldButton->setObjectName(QStringLiteral("oldButton"));

        verticalLayout_3->addWidget(oldButton);


        verticalLayout_4->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(Widget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        androidCheck = new QCheckBox(groupBox_2);
        androidCheck->setObjectName(QStringLiteral("androidCheck"));

        verticalLayout_2->addWidget(androidCheck);

        iosCheck = new QCheckBox(groupBox_2);
        iosCheck->setObjectName(QStringLiteral("iosCheck"));

        verticalLayout_2->addWidget(iosCheck);


        verticalLayout_4->addWidget(groupBox_2);

        groupBox = new QGroupBox(Widget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        macCheck = new QCheckBox(groupBox);
        macCheck->setObjectName(QStringLiteral("macCheck"));

        verticalLayout->addWidget(macCheck);

        linuxCheck = new QCheckBox(groupBox);
        linuxCheck->setObjectName(QStringLiteral("linuxCheck"));

        verticalLayout->addWidget(linuxCheck);

        winCheck = new QCheckBox(groupBox);
        winCheck->setObjectName(QStringLiteral("winCheck"));

        verticalLayout->addWidget(winCheck);


        verticalLayout_4->addWidget(groupBox);

        applyButton = new QPushButton(Widget);
        applyButton->setObjectName(QStringLiteral("applyButton"));

        verticalLayout_4->addWidget(applyButton);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
        fillButton->setText(QApplication::translate("Widget", "Fill in for me", 0));
        groupBox_3->setTitle(QApplication::translate("Widget", "Radio Button Group", 0));
        newButton->setText(QApplication::translate("Widget", "new", 0));
        oldButton->setText(QApplication::translate("Widget", "o&ld", 0));
        groupBox_2->setTitle(QApplication::translate("Widget", "nonExclusive", 0));
        androidCheck->setText(QApplication::translate("Widget", "Android", 0));
        iosCheck->setText(QApplication::translate("Widget", "iOS", 0));
        groupBox->setTitle(QApplication::translate("Widget", "Exclusive cb", 0));
        macCheck->setText(QApplication::translate("Widget", "mac", 0));
        linuxCheck->setText(QApplication::translate("Widget", "Linux", 0));
        winCheck->setText(QApplication::translate("Widget", "windows", 0));
        applyButton->setText(QApplication::translate("Widget", "Apply", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
