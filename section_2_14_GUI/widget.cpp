#include "widget.h"
#include "ui_widget.h"
#include <QtDebug>
#include <QMessageBox>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    qDebug() << "This was clicked!";
    QMessageBox::information(this, "My message", "You Clicked Me!", QMessageBox::Ok);

}
