#include <iostream>

using namespace std;

class MyRectangle {

private:
    // Even MC Hammer can't touch this!
    int width;
    int length;

public:
    // A default constructor.
    MyRectangle();

    // Or a defined constructor.
    MyRectangle(int a, int b);

    // But main can call this function which can touch private stuff.
    void setWidth (int a){
        this->width = a;
    }
    // You can also just have it in name only, and call it as a method outside of the class.
    void setLength (int b);

    int getArea () {
        return width * length;
    }

};

// Our method call outside of class, if needed....
void MyRectangle::setLength (int b){
    this->length = b;
}

// Define our default constructor.
MyRectangle::MyRectangle(){
    clog << "Default constructor was called." << endl;
    this->width = 5;
    this->length = 10;
}

// Define our defined constructor.
// First way:
//MyRectangle::MyRectangle(int a, int b){
//    clog << "Defined constructor was called." << endl;
//    this->width = a;
//    this->length = b;
//}

// Second way: with an initializer
MyRectangle::MyRectangle(int a, int b):width(a), length(b){
    clog << "Defined constructor was called." << endl;
}

// Now we call our 3d volume.

class Para {

public:

    Para(int w, int l, int h):r(w,l),height(h){
        cout << "Called para class." << endl;
    }

    int getVol () {
        return r.getArea() * height;
    }

private:
    MyRectangle r;
    int height;

};

int main()
{
    MyRectangle r(5,11);
    Para p(5,11,3);


    cout << "My rectangle has an area of: " << r.getArea() << endl;
    cout << "My 3d para rectangle has a volume of: " << p.getVol() << endl;

    return 0;
}
