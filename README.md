# qt_course

A collection of my work from a Qt C++ class I took. If you are learning C++ or Qt, perhaps this will help you too.


# Common errors:
qt creator warning: 'auto' changes meaning in C++11; please remove it

If you have this error, here is how to fix it:

Double click on your .pro file to open it, and add this line under the CONFIG lines:

QMAKE_CXXFLAGS += -std=c++11
You can also use newer standards, such as -std=c++14, 17, etc.

Your whole .pro file will look something like this:

​TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
QMAKE_CXXFLAGS += -std=c++11
SOURCES += main.cpp

I just wanted to share this in case anyone else runs into this issue.

_______________________________________________________________________________________

If you see this:

/home/alaskalinuxuser/Documents/qt_course/section_3_17/widget.cpp:26: error: no matching function for call to 'Widget::connect(QPushButton*&, void (QAbstractButton::*)(bool), Widget::Widget(QWidget*)::<lambda()>)'
      });
       ^

Make sure you choose Qt5, not Qt4!
And set this:

QMAKE_CXXFLAGS = -std=c++14 // Or whatever standard from 11 and up.
QMAKE_LFLAGS = -std=c++14   // Or whatever standard from 11 and up.

in your .pro file!!!!
===============================================================

