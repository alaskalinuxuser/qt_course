#include "widget.h"
#include "QDebug"
#include "QMessageBox"
#include "QPushButton"

Widget::Widget(QWidget *parent) :
    QWidget(parent){
    QPushButton * showButton = new QPushButton(this);
    showButton->setText("Ok");
    showButton->move(200,200);

    connect(showButton,&QPushButton::clicked,[=](){

        // The hard way.
        /*
         * This is more controling by doing it this way.
         *
         *
        QMessageBox message;
        message.setMinimumSize(300,200);
        message.setWindowTitle("My Message.");
        message.setText("This just happened!");
        message.setInformativeText("What should we do about it?");
        message.setIcon(QMessageBox::Critical);
        message.setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
        message.setDefaultButton(QMessageBox::Cancel);

        int ret = message.exec();
        */

        // The easy way....
        // This way is faster to code, but has less control, no custom icons, etc.

        /*
         * // Critical.
        int ret = QMessageBox::critical(this,"My Title","The text, what should we do about it?",
                                        QMessageBox::Ok | QMessageBox::Cancel,QMessageBox::Cancel);
        */
        /*
        // Information.
        int ret = QMessageBox::information(this,"My Title","The information, what should we do about it?",
                                        QMessageBox::Ok | QMessageBox::Cancel,QMessageBox::Cancel);
        */
        /*
         * // Question.
        int ret = QMessageBox::question(this,"My Title","The Question, what should we do about it?",
                                        QMessageBox::Ok | QMessageBox::Cancel,QMessageBox::Cancel);

        */
        // Warning.
        int ret = QMessageBox::warning(this,"My Title","The warning, what should we do about it?",
                                        QMessageBox::Ok | QMessageBox::Cancel,QMessageBox::Cancel);

        if (ret == QMessageBox::Ok){
            qDebug() << "They clicked ok.";
        } else if (ret == QMessageBox::Cancel) {
            qDebug() << "They clicked cancel.";
        }
    });
}

Widget::~Widget()
{
    delete ui;
}
