/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton_1;
    QPushButton *pushButton_4;
    QPushButton *pushButton_3;
    QPushButton *pushButton;
    QVBoxLayout *verticalLayout_2;
    QPushButton *pushButton_d;
    QPushButton *pushButton_c;
    QPushButton *pushButton_b;
    QPushButton *pushButton_a;
    QGridLayout *gridLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton_7;
    QPushButton *pushButton_6;
    QPushButton *pushButton_5;
    QFormLayout *formLayout;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(508, 774);
        verticalLayout = new QVBoxLayout(Widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton_1 = new QPushButton(Widget);
        pushButton_1->setObjectName(QStringLiteral("pushButton_1"));

        horizontalLayout->addWidget(pushButton_1);

        pushButton_4 = new QPushButton(Widget);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));

        horizontalLayout->addWidget(pushButton_4);

        pushButton_3 = new QPushButton(Widget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));

        horizontalLayout->addWidget(pushButton_3);

        pushButton = new QPushButton(Widget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);


        verticalLayout->addLayout(horizontalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        pushButton_d = new QPushButton(Widget);
        pushButton_d->setObjectName(QStringLiteral("pushButton_d"));

        verticalLayout_2->addWidget(pushButton_d);

        pushButton_c = new QPushButton(Widget);
        pushButton_c->setObjectName(QStringLiteral("pushButton_c"));

        verticalLayout_2->addWidget(pushButton_c);

        pushButton_b = new QPushButton(Widget);
        pushButton_b->setObjectName(QStringLiteral("pushButton_b"));

        verticalLayout_2->addWidget(pushButton_b);

        pushButton_a = new QPushButton(Widget);
        pushButton_a->setObjectName(QStringLiteral("pushButton_a"));

        verticalLayout_2->addWidget(pushButton_a);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        pushButton_2 = new QPushButton(Widget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout->addWidget(pushButton_2, 1, 0, 1, 1);

        pushButton_7 = new QPushButton(Widget);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));

        gridLayout->addWidget(pushButton_7, 6, 0, 1, 1);

        pushButton_6 = new QPushButton(Widget);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));

        gridLayout->addWidget(pushButton_6, 5, 0, 1, 1);

        pushButton_5 = new QPushButton(Widget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));

        gridLayout->addWidget(pushButton_5, 2, 0, 1, 1);


        verticalLayout_2->addLayout(gridLayout);

        formLayout = new QFormLayout();
        formLayout->setSpacing(6);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        pushButton_8 = new QPushButton(Widget);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));

        formLayout->setWidget(0, QFormLayout::LabelRole, pushButton_8);

        pushButton_9 = new QPushButton(Widget);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));

        formLayout->setWidget(0, QFormLayout::FieldRole, pushButton_9);

        pushButton_10 = new QPushButton(Widget);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));

        formLayout->setWidget(1, QFormLayout::LabelRole, pushButton_10);

        pushButton_11 = new QPushButton(Widget);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));

        formLayout->setWidget(1, QFormLayout::FieldRole, pushButton_11);


        verticalLayout_2->addLayout(formLayout);


        verticalLayout->addLayout(verticalLayout_2);


        retranslateUi(Widget);

        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
        pushButton_1->setText(QApplication::translate("Widget", "1", 0));
        pushButton_4->setText(QApplication::translate("Widget", "2", 0));
        pushButton_3->setText(QApplication::translate("Widget", "3", 0));
        pushButton->setText(QApplication::translate("Widget", "4", 0));
        pushButton_d->setText(QApplication::translate("Widget", "d", 0));
        pushButton_c->setText(QApplication::translate("Widget", "c", 0));
        pushButton_b->setText(QApplication::translate("Widget", "b", 0));
        pushButton_a->setText(QApplication::translate("Widget", "a", 0));
        pushButton_2->setText(QApplication::translate("Widget", "t", 0));
        pushButton_7->setText(QApplication::translate("Widget", "w", 0));
        pushButton_6->setText(QApplication::translate("Widget", "v", 0));
        pushButton_5->setText(QApplication::translate("Widget", "u", 0));
        pushButton_8->setText(QApplication::translate("Widget", "6", 0));
        pushButton_9->setText(QApplication::translate("Widget", "7", 0));
        pushButton_10->setText(QApplication::translate("Widget", "8", 0));
        pushButton_11->setText(QApplication::translate("Widget", "9", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
