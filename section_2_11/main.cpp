#include <iostream>

using namespace std;

class MyRectangle {

private:
    // Even MC Hammer can't touch this!
    int width;
    int length;

public:
    // But main can call this function which can touch private stuff.
    void setWidth (int a){
        this->width = a;
    }
    // You can also just have it in name only, and call it as a method outside of the class.
    void setLength (int b);

    int getArea () {
        return width * length;
    }

};

// Our method call outside of class, if needed....
void MyRectangle::setLength (int b){
    this->length = b;
}

int main()
{
    MyRectangle r;
    r.setWidth(5);
    r.setLength(4);

    cout << "My rectangle has an area of: " << r.getArea() << endl;
    return 0;
}

