#-------------------------------------------------
#
# Project created by QtCreator 2018-06-25T09:46:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = section_4_19
TEMPLATE = app


SOURCES += main.cpp \
    rockwidget.cpp

HEADERS  += \
    rockwidget.h

FORMS    +=
