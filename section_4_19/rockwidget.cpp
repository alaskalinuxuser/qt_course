#include "rockwidget.h"
#include "QLabel"
#include "QFont"
#include "QPushButton"
#include "QMessageBox"

RockWidget::RockWidget(QWidget *parent) : QWidget(parent)
{
    // Do this here, or in main.cpp.
    // setWindowTitle("My Rock Widget!");

    // Add a label (QLabel text).
    QLabel * label1 = new QLabel("My rockin' label",this);

    //Define my font.
    QFont labelFont("Times", 30, QFont::Bold);

    // Define a color palette for the label.
    QPalette labelPalette;

    labelPalette.setColor(QPalette::Window,Qt::red);
    labelPalette.setColor(QPalette::WindowText,Qt::white);


    // Add styles widget and move it.
    QLabel * label = new QLabel(this);
    // Set the text of the label.
    label->setText("My color label.");
    // Move that label 50 pixels left, then 50 pixels down.
    label->move(50,50);
    // change the font.
    label->setFont(labelFont);
    // choose the palette color for the text.
    label->setPalette(labelPalette);
    // set the palette for the background of the text.
    label->setAutoFillBackground(true);


    // Now we will add a button and connect it to a slot.
    QPushButton * myButton = new QPushButton(this);
    // Add the font to it
    myButton->setFont(labelFont);
    // Set the text
    myButton->setText("My Button");
    // Move the button
    myButton->move(80,180);
    // And connect the button to the slot.
    connect(myButton,SIGNAL(clicked()), this,SLOT (myButtonClicked()));



}

void RockWidget::myButtonClicked()
{
    QMessageBox::information(this,"Message Title","Message Text.");
}

QSize RockWidget::sizeHint() const
{   // A way to specify the start size of the widget.
    return QSize(500,600);
}

