#include <rockwidget.h>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    RockWidget w;
    // You can do this here, or in the RockWidget.cpp file.
    w.setWindowTitle("My Widget Rocks!");
    w.show();

    return a.exec();
}
