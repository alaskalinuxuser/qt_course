#include <iostream>
#include <string>

using namespace std;

int main()
{
    // Use cout to put a stream out to a console.
    cout << "Hello World!" << endl;

    // Use cerr to put output stream of error to the console.
    cerr << "There is an error." << endl;

    // Use clog to output logs to the console.
    clog << "Here is a log message." << endl;

    // Use cin to get input from the console.
    // First make a variable for a string.
    string myName;
    // Ask the user for thier name....
    cout << "What is your name? ";
    cin >> myName;
    cout << "I like that name, " << myName << ", it is a great name!" << endl;

    return 0;
}

