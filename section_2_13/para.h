#ifndef PARA
#define PARA

#include <iostream>
#include "myrectangle.h"

using namespace std;

// Now we call our 3d volume.

class Para {

public:

    Para(int w, int l, int h):r(w,l),height(h){
        cout << "Called para class." << endl;
    }

    int getVol () {
        return r.getArea() * height;
    }

private:
    MyRectangle r;
    int height;

};

#endif // PARA

