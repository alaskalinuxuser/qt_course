#ifndef SQUARE
#define SQUARE

#include <iostream>
#include "myrectangle.h"

using namespace std;



// Make a class that extends and inherits from another class.
class Square : public MyRectangle {
public:
    Square (int side) : MyRectangle (side,side){}
};

#endif // SQUARE

