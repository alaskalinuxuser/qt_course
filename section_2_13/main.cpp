#include <iostream>

// We are including these header files with our defined class for rectangle para and square!
#include "myrectangle.h"
#include "para.h"
#include "square.h"

using namespace std;


int main()
{
    MyRectangle r(9,11);
    Para p(5,12,3);
    Square s(38);

    cout << "My rectangle has an area of: " << r.getArea() << endl;
    cout << "My 3d para rectangle has a volume of: " << p.getVol() << endl;

    // Square class doesn't have an area method, but Rectangle does, and square inherits Rectangle.
    cout << "My square has an area of: " << s.getArea() << endl;

    return 0;
}
