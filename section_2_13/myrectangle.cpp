// Use quotes for local files you made.
#include "myrectangle.h"
// Use less/greater than for system imports.
#include <iostream>

using namespace std;

// Our method call outside of class, if needed....
void MyRectangle::setLength (int b){
    this->length = b;
}

// Define our default constructor.
MyRectangle::MyRectangle(){
    clog << "Default constructor was called." << endl;
    this->width = 5;
    this->length = 10;
}

// Define our defined constructor.
// First way:
//MyRectangle::MyRectangle(int a, int b){
//    clog << "Defined constructor was called." << endl;
//    this->width = a;
//    this->length = b;
//}

// Second way: with an initializer
MyRectangle::MyRectangle(int a, int b):width(a), length(b){
    clog << "Defined constructor was called." << endl;
}
