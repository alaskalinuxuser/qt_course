#ifndef MYRECTANGLE
#define MYRECTANGLE


class MyRectangle {

private:
    // Even MC Hammer can't touch this!
    int width;
    int length;

public:
    // A default constructor.
    MyRectangle();

    // Or a defined constructor.
    MyRectangle(int a, int b);

    // But main can call this function which can touch private stuff.
    void setWidth (int a){
        this->width = a;
    }
    // You can also just have it in name only, and call it as a method outside of the class.
    void setLength (int b);

    int getArea () {
        return width * length;
    }

};

#endif // MYRECTANGLE
