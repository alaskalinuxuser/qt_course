#include <iostream>

using namespace std;

int main()
{


//    // First Method
//    // Create a lambda
//    auto lamdbaFunction = [](){cout << "Hello World!" << endl;
//    };

//    // And we call it.
//    lamdbaFunction();

//    // Second method, create and call at one time.
//    [](){cout << "Hello World!" << endl;
//        };

//        // Or call one with parameters.
//        [](int a, int b){
//            cout << "Product of a and b: " << a*b << endl;
//        }(8,2);

//    // Or have a lambda that returns something.
//    int product = [](int c, int d)->int{
//        return c*d;
//    }(7,4);
//    cout << "Product of c and d: " << product << endl;

//    // Or call it inside of what you want to do:
//    cout << "Product of e and f: " << [](int e, int f)->int{
//        return e*f;
//    }(73,2) << endl;


//    // Or capture a list of variables that were outside of our lambda.
//    int g = 18;
//    int h = 21;

//    [g,h](){
//        cout << "g plus h is: " << g+h << endl;
//    }(); // Don't forget these parenthesis at the end!


    // Capture by a value: This is a copy of the i value.
    // So the original variable never changes, even though you can change the copy of it.
    int i=14;

    auto func = [i]() {
        cout << "value of i inside function: " << i << endl;
    };

    for (int j = 1; j < 5; j++){
        func();
        i++;
        cout << "value of i outside function: " << i << endl;
    }

    // Capture by a reference: This is a reference (use the original) k value.
    // So this one edits the actual referenced variable itself.
    int k=29;

    auto func2 = [&k]() {
        cout << "value of k inside function: " << k << endl;
    };

    for (int l = 1; l < 5; l++){
        func2();
        k++;
        cout << "value of k outside function: " << k << endl;
    }

    // Capture everything by value.
    // So the original variable never changes, even though you can change the copy of it.
    int m=32;
    int n=21;

    auto func3 = [=]() {
        cout << "value of m and n inside function: " << m << " " << n << endl;
    };

    for (int l = 1; l < 5; l++){
        func3();
        m++; n++;
        cout << "value of m and n oustide function: " << m << " " << n << endl;
    }

    // Or capture everything by reference!
    // So this one edits the actual referenced variable itself.
    int o=89;
    int p=76;

    auto func4 = [&]() {
        cout << "value of o and p inside function: " << o << " " << p << endl;
    };

    for (int l = 1; l < 5; l++){
        func4();
        o++; p++;
        cout << "value of o and p oustide function: " << o << " " << p << endl;
    }



    return 0;
}

