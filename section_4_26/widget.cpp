#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->imageLabel->move(100,100);

    // Here we add the pixel map with the resource path.
    // Use copy resource rather than type it.
    QPixmap gbPixmap(":/new/prefix1/images/gb.jpeg");

    ui->imageLabel->setPixmap(gbPixmap.scaled(400,400));

}

Widget::~Widget()
{
    delete ui;
}
