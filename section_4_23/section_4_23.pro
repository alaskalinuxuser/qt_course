#-------------------------------------------------
#
# Project created by QtCreator 2018-06-28T13:00:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = section_4_23
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++14



SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
