#include "widget.h"
#include "QPushButton"
#include "QDebug"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{
    QFont buttonFont("Times", 20, QFont::Bold);
    QPushButton * oneButton = new QPushButton(this);
    oneButton->setFont(buttonFont);
    oneButton->setText("Click");
    oneButton->setMinimumSize(200,200);
    connect(oneButton,&QPushButton::clicked,[=](){
       qDebug() << "Button one was clicked.";
    });

    QPushButton * twoButton = new QPushButton(this);
    twoButton->setFont(buttonFont);
    twoButton->setText("Press");
    twoButton->move(200,0);
    twoButton->setMinimumSize(200,200);
    connect(twoButton,&QPushButton::pressed,[=](){
       qDebug() << "Button one was pressed.";
    });

    QPushButton * threeButton = new QPushButton(this);
    threeButton->setFont(buttonFont);
    threeButton->setText("Released");
    threeButton->move(400,0);
    threeButton->setMinimumSize(200,200);
    connect(threeButton,&QPushButton::released,[=](){
       qDebug() << "Button one was released.";
    });

}

Widget::~Widget()
{

}
