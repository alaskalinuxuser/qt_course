#include <iostream>
#include <string>

using namespace std;

int main()
{
    string howdyStrin = "Hello World!!";
    int myCount = 5;
    cout << "I want to say: " << howdyStrin << endl;
    cout << "And I want to say it " << myCount << " times!" << endl;

    cout << "Size of int is: " << sizeof(int) << endl;
    cout << "Size of float is: " << sizeof(float) << endl;
    cout << "Size of double is: " << sizeof(double) << endl;
    cout << "Size of char is: " << sizeof(char) << endl;
    cout << "Size of howdyString is: " << sizeof(howdyStrin) << endl;



    cout << "Size of short int is: " << sizeof(short int) << endl;

    unsigned int myVariable = -200; // This is bad, and will give a bad output, because
    // it should only be a positive in an unsigned int!

    cout << "myVariable is: " << myVariable << endl;

    return 0;
}

