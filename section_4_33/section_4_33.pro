QT += core
QT -= gui

TARGET = section_4_33
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app
QMAKE_CXXFLAGS = -std=c++14
QMAKE_LFLAGS = -std=c++14

SOURCES += main.cpp

