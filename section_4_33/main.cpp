#include <QCoreApplication>
#include <QDebug>
#include <QList>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<QString> myStringList;

    myStringList << "Howdy."
                 << " I "
                 << " like this"
                 << " course on Qt.";

    myStringList.append(" I hope");
    myStringList.append(" you do too!");

    //qDebug() << myStringList[1];

    for (int i=0; i < myStringList.count(); i++) {
        qDebug() << myStringList[i];
    }

    return a.exec();
}

