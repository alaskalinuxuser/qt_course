#-------------------------------------------------
#
# Project created by QtCreator 2018-07-10T07:40:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = section_4_25
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++14


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h
