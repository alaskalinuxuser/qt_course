#include "widget.h"
#include "QTextEdit"
#include "QLabel"
#include "QDebug"
#include "QPushButton"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

    setupUI();

}

Widget::~Widget()
{

}

void Widget::setupUI()
{
    QFont myLabelFont("times", 20, QFont::Bold);
    QLabel * myLabel = new QLabel("My Text",this);
    myLabel->setFont(myLabelFont);
    myLabel->move(10,10);

    QTextEdit * myText = new QTextEdit(this);
    myText->move (70, 55);

    //Text Changed
    connect(myText,&QTextEdit::textChanged,[=](){
        qDebug() << "Text was changed.";
    });

    // Make some buttons for copy/paste/cut.
    QPushButton * copyButton = new QPushButton("Copy",this);
    copyButton->setMinimumSize(50,25);
    copyButton->move(10,250);

    QPushButton * pasteButton = new QPushButton("Paste",this);
    pasteButton->setMinimumSize(50,25);
    pasteButton->move(100,250);

    QPushButton * cutButton = new QPushButton("Cut",this);
    cutButton->setMinimumSize(50,25);
    cutButton->move(190,250);

    // Now connect the buttons to functions.
    connect(copyButton,&QPushButton::clicked,[=](){
        myText->copy();
    });

    connect(cutButton,&QPushButton::clicked,[=](){
        myText->cut();
    });

    connect(pasteButton,&QPushButton::clicked,[=](){
        myText->paste();
    });

    // Now undo and redo
    QPushButton * undoButton = new QPushButton("Undo",this);
    undoButton->setMinimumSize(50,25);
    undoButton->move(10,300);

    QPushButton * redoButton = new QPushButton("Redo",this);
    redoButton->setMinimumSize(50,25);
    redoButton->move(100,300);

    connect(undoButton,&QPushButton::clicked,[=](){
        myText->undo();
    });

    connect(redoButton,&QPushButton::clicked,[=](){
        myText->redo();
    });

    // Setting text to plain or html
    QPushButton * htmlButton = new QPushButton("Enter HTML",this);
    htmlButton->setMinimumSize(50,25);
    htmlButton->move(190,300);

    connect(htmlButton,&QPushButton::clicked,[=](){
        myText->setHtml("<h1>HTML Text</h1><p>This Text is formated in HTML</p>");
    });

    // Grab text plain or html
    QPushButton * grabTextButton = new QPushButton("Grab Text",this);
    grabTextButton->setMinimumSize(50,25);
    grabTextButton->move(10,350);

    connect(grabTextButton,&QPushButton::clicked,[=](){
        qDebug() << "This is myText plain: " << myText->toPlainText();
    });

    QPushButton * grabHTMLButton = new QPushButton("Grab HTML",this);
    grabHTMLButton->setMinimumSize(50,25);
    grabHTMLButton->move(100,350);

    connect(grabHTMLButton,&QPushButton::clicked,[=](){
        qDebug() << "This is myText HTML: " << myText->toHtml();
    });

    // Set your wigdets fixed size.
    setFixedSize(400,400);
}
