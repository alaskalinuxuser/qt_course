#include <iostream>

using namespace std;


void helloWorld () {
    cout << "Hello World!" << endl;
}

int numberFunction (int a, int b) {
    return a+b;
}

float numberFunction (float a, float b) {
    return a+b;
}

double numberFunction (double a, double b) {
    return a+b;
}

int main()
{
    helloWorld();
    cout << numberFunction(6,10) << endl;
    cout << numberFunction(6.2f,10.2f) << endl;
    cout << numberFunction(6.3,10.2) << endl;
    helloWorld();
    return 0;
}



