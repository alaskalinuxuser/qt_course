#include "widget.h"
#include "QLabel"
#include "QDebug"
#include "QLineEdit"
#include "QPushButton"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
{

    //City
    QLabel * cityQLabel = new QLabel("City", this);
    cityQLabel->setMinimumSize(100,50);
    cityQLabel->move(10,10);

    QLineEdit * cityQLineEdit = new QLineEdit(this);
    cityQLineEdit->setMinimumSize(200,50);
    cityQLineEdit->move(125,10);

    //State or county
    QLabel * stateQLabel = new QLabel("State or County", this);
    stateQLabel->setMinimumSize(100,50);
    stateQLabel->move(10,70);

    QLineEdit * stateQLineEdit = new QLineEdit(this);
    stateQLineEdit->setMinimumSize(200,50);
    stateQLineEdit->move(125,70);

    //Country
    QLabel * countryQLabel = new QLabel("Country", this);
    countryQLabel->setMinimumSize(100,50);
    countryQLabel->move(10,130);

    QLineEdit * countrtyQLineEdit = new QLineEdit(this);
    countrtyQLineEdit->setMinimumSize(200,50);
    countrtyQLineEdit->move(125,130);

    //Continent
    QLabel * continentQLabel = new QLabel("Continent", this);
    continentQLabel->setMinimumSize(100,50);
    continentQLabel->move(10,190);

    QLineEdit * continentQLineEdit = new QLineEdit(this);
    continentQLineEdit->setMinimumSize(200,50);
    continentQLineEdit->move(125,190);

    //Planet
    QLabel * planetQLabel = new QLabel("Planet", this);
    planetQLabel->setMinimumSize(100,50);
    planetQLabel->move(10,250);

    QLineEdit * planetQLineEdit = new QLineEdit(this);
    planetQLineEdit->setMinimumSize(200,50);
    planetQLineEdit->move(125,250);

    // Add a button to take form data....
    QFont buttonFont("Times", 20, QFont::Bold);
    QPushButton * grabButton = new QPushButton("Submit data", this);
    grabButton->setFont(buttonFont);
    grabButton->move(125,310);

    // Now connect it....
    connect(grabButton,&QPushButton::clicked,[=](){
        QString cityString = cityQLineEdit->text();
        QString stateString = stateQLineEdit->text();
        QString countryString = countrtyQLineEdit->text();
        QString continentString = continentQLineEdit->text();
        QString planetString = planetQLineEdit->text();

        if (!cityString.isEmpty() && !stateString.isEmpty() &&
                !countryString.isEmpty() && !continentString.isEmpty() &&
                !planetString.isEmpty()) {
        qDebug() << "City: " << cityString;
        qDebug() << "State: " << stateString;
        qDebug() << "Country: " << countryString;
        qDebug() << "Continent: " << continentString;
        qDebug() << "Planet: " << planetString;
        } else {
            // They did not fill everything in! Tell them!
            qDebug() << "Form is not complete. Please it in.";
        }

    });

    // Coursor position.
    connect(cityQLineEdit, &QLineEdit::cursorPositionChanged,[=](){
        // Tell us how many characters from start that the cursor is:
        qDebug() << "Current position is: " << cityQLineEdit->cursorPosition();
    });

    // Editing Finished.
    connect(stateQLineEdit, &QLineEdit::editingFinished,[=](){
        qDebug() << "Editing State is finished";
    });

    // Return pressed.
    connect(countrtyQLineEdit, &QLineEdit::returnPressed,[=](){
        qDebug() << "Return was pressed on country.";
    });

    // Selection Changed.
    connect(continentQLineEdit, &QLineEdit::selectionChanged,[=](){
        qDebug() << "Selection was changed.";
    });

    // Text Changed.
    connect(planetQLineEdit, &QLineEdit::textChanged,[=](){
        qDebug() << "Text was changed to: " << planetQLineEdit->text();
    });

    // Text Edited.
    connect(cityQLineEdit, &QLineEdit::textEdited,[=](){
        // Very similar to textChanged.
        qDebug() << "Text was Edited to: " << cityQLineEdit->text();
    });

    // Change the text via programing
    planetQLineEdit->setText("Earth");

    // Enter hint text....
    cityQLineEdit->setPlaceholderText("Enter your City");


}

Widget::~Widget()
{

}
