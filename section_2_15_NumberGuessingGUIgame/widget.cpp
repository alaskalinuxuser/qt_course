#include "widget.h"
#include "ui_widget.h"
#include <stdlib.h>
#include <time.h>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{

    ui->setupUi(this);
    ui->guessButton->setDisabled(false);
    ui->startOverButton->setDisabled(true);
    // Get our random number generator ready.
    srand (time(NULL));

    // Make the random number.
    hiddenNum = rand() % 10 +1;

    qDebug() << "Hidden number is: " << QString::number(hiddenNum);

    ui->spinBox->setValue(1);

    ui->messageLabel->setText(" ");
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_guessButton_clicked()
{
    guessNum = ui ->spinBox ->value();
    qDebug() << "Guessed number is: " << QString::number(guessNum);

    if (guessNum == hiddenNum){
        ui->messageLabel->setText("Congratulations! You guessed it!");
        // And disable the button.
        ui->guessButton->setDisabled(true);
        ui->startOverButton->setDisabled(false);
    } else if (guessNum > hiddenNum) {
        ui->messageLabel->setText("Oops! The number is less than that!");

    } else {
        ui->messageLabel->setText("Oops! The number is greater than that!");

    }
}

void Widget::on_startOverButton_clicked()
{
    ui->guessButton->setDisabled(false);
    ui->startOverButton->setDisabled(true);
    // Get our random number generator ready.
    srand (time(NULL));

    // Make the random number.
    hiddenNum = rand() % 10 +1;

    qDebug() << "Hidden number is: " << QString::number(hiddenNum);

    ui->spinBox->setValue(1);

    ui->messageLabel->setText(" ");

}

