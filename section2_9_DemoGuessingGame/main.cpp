#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int myGuess = 0;
    int randomNumber = 0;

    srand(time(NULL));
    randomNumber = rand() %10 + 1;

    cout << "Guess my number between 1 and 10: ";
    do{
        cin >> myGuess;
        if (myGuess < randomNumber) {
            cout << "myGuess < randomNumber." << endl;
        } else if (myGuess > randomNumber) {
            cout << "myGuess > randomNumber." << endl;
        }
    }while (myGuess != randomNumber);

       cout << "Great job! You guessed it!" << endl;

}

